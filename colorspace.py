"""
Created on Tue Nov 12 18:53:13 2019

@author: idahoo
"""

import numpy as np
import math


class CIE1931:
    def __init__(self, e, r, obs):
        self.e = np.float64(e)
        if r.max()>1:
            self.r = r/100
        self.r = np.float64(r)
        self.obs = np.float64(obs)

    def XYZ(self):
        xbar = np.float64(self.obs[:, 0])
        ybar = np.float64(self.obs[:, 1])
        zbar = np.float64(self.obs[:, 2])
        k = 100 / np.dot(ybar, self.e)
        X = k * sum(self.e * xbar * self.r)
        Y = k * sum(self.e * ybar * self.r)
        Z = k * sum(self.e * zbar * self.r)
        return (X, Y, Z)

    def xyz(self, X, Y, Z):
        X = np.float64(X)
        Y = np.float64(Y)
        Z = np.float64(Z)
        x = X / (X + Y + Z)
        y = Y / (X + Y + Z)
        z = Z / (X + Y + Z)
        return x, y, z

    def xyz_direct(self):
        X, Y, Z = CIE1931(self.e, self.r, self.obs).XYZ()
        x = X / sum([X, Y, Z])
        y = Y / sum([X, Y, Z])
        z = Z / sum([X, Y, Z])
        return x, y, z

    @staticmethod
    def deltae(XYZ1, XYZ2):
        X1 = np.float64(XYZ1[0])
        Y1 = np.float64(XYZ1[1])
        Z1 = np.float64(XYZ1[2])
        X2 = np.float64(XYZ2[0])
        Y2 = np.float64(XYZ2[1])
        Z2 = np.float64(XYZ2[2])
        deltae = (((X1 - X2) ** 2) + ((Y1 - Y2) ** 2) + ((Z1 - Z2) ** 2)) ** 0.5
        return deltae


class CIE1976:
    def __init__(self, e, r, obs):
        self.e = np.float64(e)
        if r.max()>1:
            self.r = r/100
        self.r = np.float64(r)
        self.obs = np.float64(obs)

    def lab_direct(self):
        k = 100 / np.dot(self.obs[:, 1], self.e)
        X = k * sum(self.e * self.obs[:, 0] * (self.r ** (1 / 3)))
        Y = k * sum(self.e * self.obs[:, 1] * (self.r ** (1 / 3)))
        Z = k * sum(self.e * self.obs[:, 2] * (self.r ** (1 / 3)))
        XS = k * sum(self.e * self.obs[:, 0])
        YS = 100
        ZS = k * sum(self.e * self.obs[:, 2])
        LS = (116 * ((Y / YS)) ** (1 / 3)) - (16)
        aS = 500 * (((X / XS) ** (1 / 3)) - ((Y / YS)) ** (1 / 3))
        bS = 200 * (((Y / YS) ** (1 / 3)) - ((Z / ZS)) ** (1 / 3))
        return LS, aS, bS

    def lab(self, X, Y, Z):
        k = 100 / np.dot(self.obs[:, 1], self.e)
        XS = k * sum(self.e * self.obs[:, 0])
        YS = 100
        ZS = k * sum(self.e * self.obs[:, 2])
        LS = (116 * ((Y / YS)) ** (1 / 3)) - (16)
        aS = 500 * (((X / XS) ** (1 / 3)) - ((Y / YS)) ** (1 / 3))
        bS = 200 * (((Y / YS) ** (1 / 3)) - ((Z / ZS)) ** (1 / 3))
        return LS, aS, bS

    @staticmethod
    def deltae(lab1, lab2):
        LS1 = np.float64(lab1[0])
        aS1 = np.float64(lab1[1])
        bS1 = np.float64(lab1[2])
        LS2 = np.float64(lab2[0])
        aS2 = np.float64(lab2[1])
        bS2 = np.float64(lab2[2])
        deltae = (((LS1 - LS2) ** 2) + ((aS1 - aS2) ** 2) + ((bS1 - bS2) ** 2)) ** 0.5
        return (deltae)
