class SPECTROPHOTOMETRIC:
    def __init__(self, K, S, std, l ,w):
        # Calculate the concentration Weight matrix
        # The *STD symbol is used to summarize instead of KOS_STD
        # enter the length of wavelength as L
        self.K = np.float64(K)
        self.S = np.float64(S)


    def S_C(self, l , std):
        # Calculate the concentration
        # The *STD symbol is used to summarize instead of KOS_STD
        # enter the length of wavelength as L
        std = np.float64(std)
        l = np.float64(l)
        CCOEFS = np.ones([l + 1, 4])
        OBS = np.hstack((np.zeros(l), 1))
        CCOEFS[:-1, 0] = self.K[:, 0] - self.S[:, 0] * std
        CCOEFS[:-1, 1] = self.K[:, 1] - self.S[:, 1] * std
        CCOEFS[:-1, 2] = self.K[:, 2] - self.S[:, 2] * std
        CCOEFS[:-1, 3] = self.K[:, 3] - self.S[:, 3] * std
        C = np.linalg.pinv(CCOEFS.T.dot(CCOEFS)).dot(CCOEFS.T).dot(OBS)
        return (C)

    def S_CW(self, w):
        std = np.float64(std)
        l = np.float64(l)
        CCOEFS = np.ones([l + 1, 4])
        OBS = np.hstack((np.zeros(l), 1))
        CCOEFS[:-1, 0] = self.K[:, 0] - self.S[:, 0] * std
        CCOEFS[:-1, 1] = self.K[:, 1] - self.S[:, 1] * std
        CCOEFS[:-1, 2] = self.K[:, 2] - self.S[:, 2] * std
        CCOEFS[:-1, 3] = self.K[:, 3] - self.S[:, 3] * std
        C = np.linalg.pinv(CCOEFS.T.dot(w).dot(CCOEFS)).dot(CCOEFS.T).dot(w).dot(OBS)
        return (C)

    def S_KAS(self, C):
        C = np.float64(C)
        KOS_Mix = np.float64([sum(i) for i in (C * self.K)]) / np.float64([sum(i) for i in (C * self.S)])
        sum(C * self.K) / sum(C * self.S)
        return (KOS_Mix)


'''
# =============================================================================
# colorimetric color formulation based on two-constant
# =============================================================================
'''

class COLORIMETRIC:
    @staticmethod
    def C2_C(K_STD, R_STD, KANDS, E, OBS):
        # KANDS is for colored sampel
        K_STD = np.float64(K_STD)
        R_STD = np.float64(R_STD)
        KANDS = np.float64(KANDS)
        E = np.float64(E)
        OBS = np.float64(OBS)
        S_STD = 1
        K4 = np.vstack((KANDS[:, 3]))
        S4 = np.vstack((KANDS[:, 7]))
        Phi_K = KANDS[:, 0:3]
        Phi_S = KANDS[:, 4:7]
        u = np.vstack((1, 1, 1))
        D_K = np.diag(-2 * (R_STD ** 2) / (1 - R_STD ** 2))
        D_S = np.diag(R_STD * (1 - R_STD) / (1 + R_STD))
        E = np.diag(E)
        a = np.linalg.pinv(OBS.dot(E).dot(D_K.dot(Phi_K - K4.dot(u.T)) + D_S.dot(Phi_S - S4.dot(u.T))))
        C_Cal = a.dot(OBS).dot(E).dot(D_K.dot(K_STD - KANDS[:, 3]) + D_S.dot(S_STD - KANDS[:, 7]))
        return (C_Cal)