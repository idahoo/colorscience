import numpy as np
from colorspace import CIE1931

class BERNS:
    def __init__(self, start, end, step, numberofsamples, XYZ, sig=62.5, m_rgb=[440, 450, 603]):
        self.sig = sig
        self.m_rgb = m_rgb
        self.start = start
        self.numberofsamples = numberofsamples
        self.x = np.linspace(start, end, ((end - start) / step) + 1)
        self.B = np.exp((-(self.x - self.m_rgb[0]) ** 2) / ((self.sig ** 2) * 2))
        self.G = np.exp((-(self.x - self.m_rgb[1]) ** 2) / ((self.sig ** 2) * 2))
        self.R = np.exp((-(self.x - self.m_rgb[2]) ** 2) / ((self.sig ** 2) * 2))
        self.XYZ = XYZ

    def berns(self):
        V_BGR = np.vstack([self.R, self.G, self.B]).T
        LAB_B_D65 = np.zeros([3, self.numberofsamples])
        LAB_B_A = np.zeros([3, self.numberofsamples])
        LAB_B_F11 = np.zeros([3, self.numberofsamples])
        DeltaE_B_D65 = np.zeros([1, self.numberofsamples])
        DeltaE_B_A = np.zeros([1, self.numberofsamples])
        DeltaE_B_F11 = np.zeros([1, self.numberofsamples])
        a = np.vstack([[CIE1931.XYZ(self.e, self.R, self.obs)], [CIE1931.XYZ(self.e, self.G, self.obs)],
                       [CIE1931.XYZ(self.e, self.B, self.obs)]]).T
        C = np.linalg.inv(a).dot(self.XYZ)
        return V_BGR.dot(C)


class HAWKYARD:
    def __init__(self, start, end, step, numberofsamples, XYZ, LAB, e, obs):
        self.start = start
        self.end = end
        self.step = step
        self.numberofsamples = numberofsamples
        self.L = [x for x in range(start, end, step)]
        self.XYZ = XYZ
        self.LAB = LAB
        self.e = e
        self.obs = obs

    def hawkyard(self):
        Rhat = []
        count = []
        T = []
        C = []
        xx = []
        yy = []

        for i in range(self.numberofsamples):
            T.append(self.XYZ)
            C.append(self.LAB)

        K = 100 / sum(self.e * self.obs[:, 1])

        Xn = K * sum(self.e * self.obs[:, 0])
        Yn = K * sum(self.e * self.obs[:, 1])
        Zn = K * sum(self.e * self.obs[:, 2])

        W = K * np.array([(self.e * self.obs[:, 0]), (self.e * self.obs[:, 1]), (self.e * self.obs[:, 2])])

        R_H = np.zeros([((self.end - self.start) / self.step) + 1, self.numberofsamples])
        for j in range(0, (self.numberofsamples)):
            DELTAXYZ = 100
            Xold = X0 = T[j][0]
            Yold = Y0 = T[j][1]
            Zold = Z0 = T[j][2]
            counter = 0
            while DELTAXYZ > 0.001:
                r = (self.obs[:, 0] * (Xold / Xn) + self.obs[:, 1] * (Yold / Yn) + self.obs[:, 2] * (Zold / Zn)) / sum(
                    self.obs)
                Xmid = sum(W[0] * r)
                Ymid = sum(W[1] * r)
                Zmid = sum(W[2] * r)
                DELTAX = Xmid - X0
                DELTAY = Ymid - Y0
                DELTAZ = Zmid - Z0
                Xold = Xold - DELTAX
                Yold = Yold - DELTAY
                Zold = Zold - DELTAZ
                DELTAXYZ = ((DELTAX) ** 2 + (DELTAY) ** 2 + (DELTAZ) ** 2) ** (0.5)
                counter = counter + 1
            count.append(counter)
            R_H[:, j] = r
