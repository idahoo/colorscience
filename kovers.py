import numpy as np

def kos(r):
    r = np.float64(r)
    if r.max() > 1:
        r = r / 100
    kos = ((1 - r) ** 2) / (2 * r)
    return kos


def kos_mix(kos_Sub, kos_u, c):
    kos_Sub = np.float64(kos_Sub)
    kos_u = np.float64(kos_u)
    C = np.float64(c)
    kos_mix = kos_Sub + np.dot(kos_u, C)
    return (kos_mix)


def r(kos):
    kos = np.float64(kos)
    r = (1 + kos) - (((kos ** 2) + 2 * kos) ** 0.5)
    return (r)
