import numpy as np


def wal(l, kos_m, p, n):
    # enter the length of wavelength as L
    # enter the number of colored sampels as n
    KOS_M = np.float64(kos_m)
    p = np.float64(p)
    KSCOEFS = np.ones([n + 1, 8])
    KANDS = np.zeros([l, 8])
    OBS = np.hstack((np.zeros(n), 1))

    KSCOEFS[:-1, 0] = -p.T[:, 0]
    KSCOEFS[:-1, 1] = -p.T[:, 1]
    KSCOEFS[:-1, 2] = -p.T[:, 2]
    KSCOEFS[:-1, 3] = -p.T[:, 3]

    for i in range(0, 16):
        KSCOEFS[:-1, 4] = (p[0, :] * KOS_M)[i, :]
        KSCOEFS[:-1, 5] = (p[1, :] * KOS_M)[i, :]
        KSCOEFS[:-1, 6] = (p[2, :] * KOS_M)[i, :]
        KSCOEFS[:-1, 7] = (p[3, :] * KOS_M)[i, :]
        KANDS[i, :] = np.linalg.pinv(KSCOEFS.T.dot(KSCOEFS)).dot(KSCOEFS.T).dot(OBS)
    return KANDS
