import numpy as np


def gfc(act, test):
    gfc = (np.sum(act*test, axis=0))/((np.sqrt(np.sum(act**2, axis=0)))*(np.sqrt(np.sum(test**2, axis=0))))
    return gfc


def rms (act, test):
    return (sum((act - test) ** 2) / len(act)) ** 0.5